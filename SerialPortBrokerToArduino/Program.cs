﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.IO.Ports;

class Receive
{

    static SerialPort _serialPort;
    static void Main(string[] args)
    {
        _serialPort = new SerialPort();
        _serialPort.PortName = "COM9";//Set your board COM
        _serialPort.BaudRate = 9600;
        _serialPort.Open();

        var factory = new ConnectionFactory() { HostName = "172.20.10.11", UserName = "cyril", Password = "azerty" };
        using (var connection = factory.CreateConnection())
        using (var channel = connection.CreateModel())
        {
            channel.QueueDeclare(queue: "actionDevice", durable: true, exclusive: false, autoDelete: false, arguments: null);
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body;
                Command command = JsonConvert.DeserializeObject<Command>(Encoding.Default.GetString(body));
                string value = command.actionEvent;
                Console.WriteLine("Sensor state : " + value);
                _serialPort.Write(value);
            };
            channel.BasicConsume(queue: "actionDevice", autoAck: true, consumer: consumer);
            Console.WriteLine(" Press [enter] to exit");
            Console.ReadLine();
        }

        
        _serialPort.Close();


    }


    public class Command
    {
        public String sensor;
        public String actionEvent;
    }
}